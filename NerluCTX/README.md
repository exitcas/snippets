# NerluCTX
## Instalation
- Add the content of NerluCTX.js, under your `client.on("message", msg => {`
- Edit the prefix (must be 1 character only)
## How to use
- Instead of using `if (msg.content === "[prefix][command]") {`, put `if (command === "[command in minuscule]") {`.
- Manipulate the array `ctx`, to get other parts of the request. You can get all the things following the command, imploding ctx, with " ".