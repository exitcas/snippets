  /* --- NerluCTX --- */
  const prefix = "[prefix]";
  /* Start */
  var ctx = msg.content;
  if (ctx.split("")[0] == prefix) {
    // Extract prefix
    ctx = ctx.split("");
    var fChar = ctx.shift();
    //console.log(ctx);
    ctx = ctx.join("");
    // Extract command
    var command = ctx.split(" ")[0].toLowerCase();
    // Extract subcommands (or ctx)
    ctx = ctx.split(" ");
    ctx.shift();
  } else {
    var command = [""];
  }
  /* End */